#!/usr/bin/env python
# -*- coding: utf-8 -*-
################################################################################
##  flex2js - Convert ArcGIS Flex Viewer maps to JSON/WebMap format
################################################################################

"""
flex2js - Convert ArcGIS Flex Viewer maps to JSON/WebMap format.
"""

import xml.etree.ElementTree as ET

import os
import sys
import json
import math

# pylint: disable=locally-disabled,missing-docstring

################################################################################

WGS84_AXIS = 6378137.0
RAD_TO_DEG = 57.295779513082323
DEG_TO_RAD = 1.0 / RAD_TO_DEG
HALF_PI = math.pi * 0.5

def web2geo(coord_x, coord_y):
    "Convert Web Mercator to degrees longitude/latitude"
    lon = math.degrees(coord_x / WGS84_AXIS)
    lon -= math.floor((lon + 180.0) / 360.0) * 360.0
    lat = math.degrees(HALF_PI - (2.0 * math.atan(math.exp(-coord_y / WGS84_AXIS))))
    return lon, lat

def geo2web(lon, lat):
    "Convert degrees longitude/latitude to Web Mercator"
    coord_x = math.radians(lon) * WGS84_AXIS
    coord_y = math.sin(math.radians(lat))
    coord_y = 0.5 * WGS84_AXIS * math.log((1.0 + coord_y) / (1.0 - coord_y))
    return coord_x, coord_y

################################################################################

def mapfunc(data, func=None, default=None):
    "Adapt a dictionary as a function"
    def impl(key):
        return data.get(func(key), default)
    return impl

################################################################################

def tojson(obj, *args, **kwargs):
    "Convert object to JSON with custom encoder."
    return json.dumps(obj, cls=JSONEncoder, sort_keys=True, *args, **kwargs)

class JSONEncoder(json.JSONEncoder):
    # pylint: disable=locally-disabled,method-hidden
    def default(self, o):
        if hasattr(o, '__json__'):
            return o.__json__()
        return json.JSONEncoder.default(self, o)

class JSONable(object):
    # pylint: disable=locally-disabled,too-few-public-methods
    def __init__(self, *args, **kwargs):
        if args:
            kwargs.update(args=args)
        vars(self).update(kwargs)

    def __str__(self):
        return '[%s: %s]' % (self.__class__.__name__, tojson(self))

    def __json__(self):
        return {
            k: v for k, v in vars(self).items()
            if v is not None and not str(k).startswith('_')
            }

################################################################################

class FlexElement(JSONable):
    def __init__(self, config, *args, **kwargs):
        self._config = config
        super(FlexElement, self).__init__(*args, **kwargs)

    def parse_attrs(self, get, tbl):
        for attr, prop, parse in tbl:
            try:
                value = get(attr)
                setattr(self, prop, None if value is None else parse(value))
            except Exception as ex:
                raise ValueError, (
                    'error parsing attribute "%s" with value "%s": %s' % (
                        attr, value, ex)), sys.exc_info()[2]

    def parse_children(self, find, tbl):
        for elem, prop, parse in tbl:
            try:
                value = find(elem)
                setattr(
                    self, prop, None if value is None else
                    parse(value) if parse else value.text
                    )
            except Exception as ex:
                raise ValueError, (
                    'error parsing elements "%s" with value "%s": %s' % (
                        elem, value, ex)), sys.exc_info()[2]

    def parse_child_list(self, find, tbl):
        for elem, prop, parse in tbl:
            try:
                value = [parse(child) if parse else child.text for child in find(elem)]
                setattr(self, prop, value if value else None)
            except Exception as ex:
                raise ValueError, (
                    'error parsing elements "%s": %s' % (
                        elem, ex)), sys.exc_info()[2]

    @staticmethod
    def parse_formatted(elem):
        if elem is not None:
            return elem.text
        return None

    parse_bool = staticmethod(mapfunc({
        'true': True, 'yes': True, '1': True,
        'false': False, 'no': False, '0': False,
        'none': None, 'null': None, 'nil': None
    }, str.lower))

################################################################################

class FieldFormat(FlexElement):
    def parse(self, elem):
        self.parse_attrs(elem.get, [
            ("precision", "places", int),
            ("usethousandsseparator", "digitsSeparator", self.parse_bool),
            ("useutc", "useUTC", self.parse_bool),
            ("dateformat", "dateFormat", str),
        ])

        return self

################################################################################

class Field(FlexElement):
    def parse(self, elem):
        self.parse_attrs(elem.get, [
            ("name", "fieldName", str),
            ("alias", "label", str),
            ("visible", "visible", self.parse_bool),
        ])

        self.parse_children(elem.find, [
            ("format", "format", self._parse_format),
        ])

        return self

    def _parse_format(self, elem):
        return FieldFormat(self._config).parse(elem)

################################################################################

class Popup(FlexElement):
    def parse(self, elem):
        self.parse_children(elem.find, [
            ('./title', 'title', self.parse_formatted),
            ('./description', 'description', self.parse_formatted),
        ])

        self.parse_child_list(elem.findall, [
            ("./fields/field", "fieldInfos", self._parse_field)
        ])

        return self

    def _parse_field(self, elem):
        return Field(self._config).parse(elem)

################################################################################

class Layer(FlexElement):
    def parse(self, elem):
        self.parse_attrs(elem.get, [
            ("id", "id", int),
            ("type", "layerType", self._parse_type),
            ("mode", "mode", self._parse_mode),
            ("label", "title", str),
            ("alpha", "opacity", float),
            ("url", "url", str),
            ("icon", "icon", str),
            ("reference", "reference", self.parse_bool),
            ("visible", "visibility", self.parse_bool),
            ("popupconfig", "popupInfo", self._parse_popup),
        ])

        self.parse_child_list(elem.findall, [
            ("./sublayer", "layers", self._parse_sublayer)
        ])

        return self

    def _parse_sublayer(self, elem):
        return Layer(self._config).parse(elem)

    def _parse_popup(self, attr):
        elem = ET.parse(os.path.join(self._config.basedir, attr))
        return Popup(self._config, url=attr).parse(elem.getroot())

    _parse_type = staticmethod(mapfunc({
        'feature': 'FeatureLayer',
        'tiled': 'ArcGISTiledMapServiceLayer',
        'dynamic': 'ArcGISDynamicMapServiceLayer',
        'osm': 'OpenStreetMapLayer',
        'label': 'LabelLayer',
    }, str.lower))

    _parse_mode = staticmethod(mapfunc({
        'ondemand': 1
    }, str.lower))

################################################################################

class Map(FlexElement):
    def parse(self, elem):
        assert elem.tag == 'map'

        self.parse_attrs(elem.get, [
            ## @todo handle more attributes
        ])

        self.parse_children(elem.find, [
            ('./basemaps', 'baseMap', self._parse_basemaps),
        ])

        self.parse_child_list(elem.findall, [
            ('./operationallayers/layer', 'operationalLayers', self._parse_oplayer),
        ])

        return self

    def _parse_basemaps(self, elem):
        basemaps = []
        last_label = None

        current = None

        for child in elem.findall('./layer'):
            this_label = child.get('label')
            if this_label != last_label:
                last_label = this_label
                current = []
                basemaps.append(current)
            current.append(child)

        if basemaps and basemaps[0]:
            layers = []
            default_basemap = {
                "baseMapLayers": layers
                }

            for child in basemaps[0]:
                layers.append(Layer(self._config).parse(child))

            default_basemap['title'] = layers[0].title

            return default_basemap

    def _parse_oplayer(self, elem):
        return Layer(self._config).parse(elem)

################################################################################

class Item(FlexElement):
    def parse(self, elem):
        self.parse_children(elem.find, [
            ('./title', 'title', self.parse_formatted),
            ('./subtitle', 'subtitle', self.parse_formatted),
            ('./map', 'extent', self._parse_extent),
        ])

        return self

    @staticmethod
    def _parse_extent(elem):
        wkid = elem.get("wkid")
        attr = elem.get("initialextent")

        extent = [float(i) for i in attr.split()[:4]]

        if wkid in ('102113', '102100', '3785', '3857'):
            return [web2geo(*extent[0:2]), web2geo(*extent[2:4])]

        return None

################################################################################

def xml2json(elem):
    result = {}

    if elem.attrib:
        result.update(elem.attrib)

    for child in elem.getchildren():
        l = result.setdefault(child.tag, [])
        l.append(xml2json(child))

    if result:
        for tag, lst in result.items():
            if len(lst) == 1:
                result[tag] = lst[0]
        return result

    elif elem.text and elem.text.strip():
        return elem.text.strip()

    return None

class WidgetConfig(FlexElement):
    def parse(self, elem):
        return xml2json(elem)

################################################################################

class Widget(FlexElement):
    def parse(self, elem):
        self.parse_attrs(elem.get, [
            ("url", "url", str),
            ("config", "config", self._parse_widgetconfig),
            ("label", "label", str),
            ("icon", "icon", str),
            ("left", "left", int),
            ("right", "right", int),
            ("top", "top", int),
            ("bottom", "bottom", int),
            ("preload", "preload", str),
        ])

        return self

    def _parse_widgetconfig(self, attr):
        elem = ET.parse(os.path.join(self._config.basedir, attr))
        return WidgetConfig(self._config).parse(elem.getroot())

################################################################################

class WidgetContainer(FlexElement):
    def parse(self, elem):
        self.parse_attrs(elem.get, [
            ("layout", "layout", str),
            ("paneltype", "panelType", str),
            ("initialstate", "initialState", str),
        ])

        self.parse_child_list(elem.findall, [
            ('./widget', 'widgets', self._parse_widget),
        ])

        return self

    def _parse_widget(self, elem):
        return Widget(self._config).parse(elem)

################################################################################

class Configuration(FlexElement):
    def __init__(self, *args, **kwargs):
        if args:
            kwargs.update(basedir=args[0])
            args = args[1:]
        super(Configuration, self).__init__(self, *args, **kwargs)

    def parse(self, root):
        elem = root.find('.')
        assert elem.tag == 'configuration'

        self.parse_children(elem.find, [
            ('.', 'item', Item(self).parse),
            ('./map', 'itemData', Map(self).parse),
        ])

        ## @todo parse widgets

        self.parse_child_list(elem.findall, [
            ('./widget', 'widgets', self._parse_widget),
            ('./widgetcontainer', 'widgetContainers', self._parse_widgetcontainer),
        ])

        return self

    def _parse_widget(self, elem):
        return Widget(self).parse(elem)

    def _parse_widgetcontainer(self, elem):
        return WidgetContainer(self).parse(elem)

################################################################################

def main():
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('flexconfig', nargs='?')
    parser.add_argument('jsconfig', nargs='?')

    parser.add_argument('--json', '-j', action='store_true', default=False,
                        help="output plain JSON (without `define' wrapper)")
    parser.add_argument('--pretty', '-p', action='store_true', default=False,
                        help='pretty-print output')

    args = parser.parse_args()

    basedir = os.getcwd()
    ifile = sys.stdin
    ofile = sys.stdout

    if args.flexconfig:
        basedir = os.path.split(args.flexconfig)[0]
        ifile = open(args.flexconfig, 'rb')

    cnf = ET.parse(ifile)

    info = Configuration(basedir=basedir).parse(cnf)

    if args.pretty:
        j = tojson(info, indent=2)
    else:
        j = tojson(info, separators=(',', ':'))

    if not args.json:
        j = 'define(%s)' % j

    if args.jsconfig:
        ofile = open(args.jsconfig, 'wb')

    ofile.write(j.encode('utf-8'))

################################################################################

if __name__ == '__main__':
    main()

################################################################################
##  EOF
################################################################################

